/*
 * Created by Island
 * Modified by keen 
 * Date: 17/03/2017
 */
#include <SoftwareSerial.h>
#define DEBUG true
SoftwareSerial sim808(7,8); 

String data[5];
String state,timegps,latitude,longitude;
        
void setup(){
  Serial.begin(19200);
  sim808.begin(9600); 
  getgps();
}

void loop(){
   sendTabData("AT+CIPGSMLOC=1,1",3000,DEBUG);
   sendTabData("AT+CGNSINF",1000,DEBUG);
   //sendData( "AT+CGNSINF",1000,DEBUG);   // returns a single NMEA sentence of GPS.
   if (state !=0) {
    Serial.println("State  :"+state);
    Serial.println("Time  :"+timegps);
    Serial.println("Latitude  :"+latitude);
    Serial.println("Longitude  :"+longitude);
    sim808.print(latitude);
    sim808.print(",");
    sim808.print (longitude);
    delay(200);
    sim808.println((char)26); // End AT command with a ^Z, ASCII code 26
    delay(5000);
    sim808.flush();
    } else {
        Serial.println("GPS Initialising...");
     }
    

}

void getgps(void){
   sendData( "AT+CGNSPWR=1",2000,DEBUG);  // sets the GPS engine ON
   Serial.println("Powered ON");
   sendData( "AT+CGPSINF=0",1000,DEBUG);  //  returns a single NMEA sentence of GPS.
}



void sendTabData(String command , const int timeout , boolean debug){
  
  String data[5]="";
  sim808.println(command);
  long int time = millis();
  int i = 0;

  while((time+timeout) > millis()){
    while(sim808.available()){
      char c = sim808.read();
      if (c != ',') {
         data[i] +=c;
         delay(100);
      } else {
        i++;  
      }
      if (i == 5) {
        delay(100);
        goto exitL;
      }
    }
  }exitL:
  if (debug) {
    state = data[1];
    timegps = data[2];
    latitude = data[3];
    longitude =data[4];  
  }
}




void sendData(String command, const int timeout, boolean debug){
    String response = "";    
    sim808.println(command); 
    delay(5);
    if(debug){
    long int time = millis();   
    while( (time+timeout) > millis()){
      while(sim808.available()){       
        response += char(sim808.read());
      }  
    }    
      Serial.print(response);
      Serial.print(response);
      Serial.print(response);
      Serial.print(response);
      Serial.print(response);
      Serial.print(response);
      Serial.print(response);
      Serial.print(response);
      Serial.print(response);
      Serial.print(response);
      Serial.print(response);
      Serial.print(response);
      Serial.print(response);
      Serial.print(response);
      Serial.print(response);
      Serial.print(response);
      Serial.print(response);
      Serial.print(response);
    }    
}
