/*


   SIM808 MQTT  - GPS  developed
  Able to turn on/off the eBike from cloudMQTT
  Boards used:
  Arduino UNO
  SIM808 shield from elecrow    for GPRS connection and GPS location

  Functions:
  - GPS data
  - Diagnostics
  - Turn on/off eBike
  - GPRS data to cloud
  - Smart print
  - CAN reading bat level

  This sketch demonstrates the capabilities of the pubsub library in combination
  with the TinyGSM - SIM808 board/library.
  It connects to an MQTT server then:
  - publishes "hello world" to the topic "outTopic" at the begining of the conexion
    check it at cloud mqtt "websocket ui"
    https://www.cloudmqtt.com/
  - subscribes to the topics for reading and publishing "TestIn", "light/bool","power/bool","battery/call" and "curstate/call"
    check the messages sent from Arduino and received also at cloud mqtt "websocket ui"
    It assumes the received payloads are strings not binary
  - Example. If the first character of the topic "light/bool" is an 1, switch ON the ESP Led
    check the actions that will occur depending of the Topic and its payload at 'callback()' function

  It will reconnect to the server if the connection is lost using a blocking
  reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
  achieve the same result without blocking the main loop.

  Library & Documentation API about PubSubClient() :
  https://pubsubclient.knolleary.net/api.html
  Library & Documentation API about TinyGSM() :
  https://github.com/vshymanskyy/TinyGSM/tree/master/src

  the WebApp is developed here: http://jgecu.surge.sh/
  Still need review in the BE&FE part.

  Code is a mix with PrototypeWemos_v5 and this web example: http://shahrulnizam.com/arduino-lesson-sim800-mqtt/


*/

/*------------- LIBRARIES Includes & Defines --------------*/
#define TINY_GSM_MODEM_SIM808
#include <TinyGsmClient.h>
#include <PubSubClient.h>
#include <SPI.h>
#include "mcp_can.h"
#include <SoftwareSerial.h>
SoftwareSerial SerialAT(7, 8); //RX,TX
#define DEBUG true

/*------------- VARIABLES Declaration --------------*/
TinyGsm modem(SerialAT);
TinyGsmClient SIMclient(modem);
PubSubClient client(SIMclient); //id



//**************FOR Printing consuming less RAM *****************//
int freeRam () {
  extern int __heap_start, *__brkval;
  int v;
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
}

void StreamPrint_progmem(Print &out, PGM_P format, ...)
{
  // program memory version of printf - copy of format string and result share a buffer
  // so as to avoid too much memory use
  char formatString[128], *ptr;
  strncpy_P( formatString, format, sizeof(formatString) ); // copy in from program mem
  // null terminate - leave last char since we might need it in worst case for result's \0
  formatString[ sizeof(formatString) - 2 ] = '\0';
  ptr = &formatString[ strlen(formatString) + 1 ]; // our result buffer...
  va_list args;
  va_start (args, format);
  vsnprintf(ptr, sizeof(formatString) - 1 - strlen(formatString), formatString, args );
  va_end (args);
  formatString[ sizeof(formatString) - 1 ] = '\0';
  out.print(ptr);
}

#define Serialprint(format, ...) StreamPrint_progmem(Serial,PSTR(format),##__VA_ARGS__)
#define Streamprint(stream,format, ...) StreamPrint_progmem(stream,PSTR(format),##__VA_ARGS__)
//**************************************************************************//



//Network details
const char apn[3]  = "online.telia.se";
const char user[1] = "";
const char pass[2] = "";


//mqtt vars
const char* mqtt_server = "m24.cloudmqtt.com" ;         // at CloudMQTT web :"Your MQTT Server IP address";
const char* mqtt_user = "rxhwkrwu" ;//"ovkfmmrv";  ovkfmmrv                   // Your user at CloudMQTT
const char* mqtt_password = "euP2FEqY5IlV";//"b6Q5wTu-3-OR";             //  Your password at CloudMQTT
const  int port =  14889;//15139;                                //just Port, not the SSL

//for control vars
byte IGNITION = 4;   //Lock Motor             //Switch PINK- GND
byte LIGHT = 3 ;     //Power just Lights      //Switch RED - YELLOW  and PINK - AIR
byte KEY = 2;        //Power eBike on         //Switch RED - YELLOW
byte T_LEFT = 5;     // Turning Light LEFT    //Switch GREY - ORANGE
byte T_RIGHT = 6;    // Turning Light RIGHT   //Switch GREY - BLUE
byte SIMPOWER = 9;

unsigned long previousMillis = 0;

//gps vars
char coord[40];
char lat[10];
char lon[10];
// gps vars SIM808
//String data[5];
String state, timegps, latitude, longitude;


//CAN batt vars
//char battstring[2];
long unsigned int rxId;
unsigned long rcvTime;
unsigned char len = 0;
unsigned char buf[8];
unsigned int bat = 0;
const int SPI_CS_PIN = 10;
MCP_CAN CAN(SPI_CS_PIN);        // Set CS pin


// sendLocationStatus and sendDiagnostic timers
uint32_t timer = millis();
uint32_t timerDiagnostics = millis();

//status and control vars of light and power
char* power = "false";
char* light = "false";

//diagnostic vars
char* breaks = "ok";
char* engine = "engine-paused health-ok";
char* ecu = "ecu-running-ok LiPo-bat-recharging";
char* batt = "f'healthd(0) battery l={batt_l} v={batt_v} t={batt_t} h=2 st=2'";


/*------------- FUNCTIONS Declaration --------------*/
void setup_wifi(void);
bool isTopic(char* topic, char* publishTopic);
char isPayload(byte* payload, char compchr);
void callback(char* topic, byte* payload, unsigned int length);
void errorMessage(void);
void batteryDebug(void);
void setup_blink(void);
void sendLocationStatus(void);
void sendDiagnostics(void);
void sendData(String command, const int timeout, boolean debug);
void subscribingMQTT();
void powerSIM(void);
void setup_switch(void);
void setup_gps(void);
void setup_can(void);


/*--------------------- SET UP  ------------------------*/
void setup() {
  Serial.begin(9600);
  SerialAT.begin(9600);
  delay(2000);
  powerSIM();
  setup_switch();
  delay(1000);
  //modem.restart();
  setup_wifi();
  client.setServer(mqtt_server, port); // server, Port (not SSL)
  client.setCallback(callback);
  delay(1000);
  setup_gps();
  delay(1000);

}

void powerSIM() {
  sendData("AT+CPOWD=1", 3000, DEBUG);
  pinMode(SIMPOWER, OUTPUT);
  digitalWrite(SIMPOWER, HIGH);
  delay(2000);
  digitalWrite(SIMPOWER, LOW);
}

void setup_switch() {
  pinMode(IGNITION, OUTPUT);
  digitalWrite(IGNITION, LOW); //not powered
  pinMode(LIGHT, OUTPUT);
  digitalWrite(LIGHT, LOW);  // not lighted
  pinMode(KEY, OUTPUT);
  digitalWrite(KEY, LOW);    // locked
  pinMode(T_LEFT, OUTPUT);   /*TODO: put the lights like this and not with the single LIGHT pin*/
  pinMode(T_RIGHT, OUTPUT);
}
void setup_gps() {
  sendData("AT+CGPSPWR=1", 2000, DEBUG); // sets the GPS engine ON
  sendData("AT+CGPSINF=0", 1000, DEBUG); //  returns a single NMEA sentence of GPS.
  Serialprint("gps");
}

void setup_can() {
  //Serial.print("Setting Up CAN");
  while (CAN_OK != CAN.begin(CAN_250KBPS))              // init can bus : baudrate = 250k for this eBike
  {
    Serialprint("CAN BUS Module Failed to Initialized");
    //Serialprint("Retrying....");
    delay(200);
  }
  Serialprint("CAN BUS Initialized");
  Serialprint("Time\t\tPGN\t\tByte0\tByte1\tByte2\tByte3\tByte4\tByte5\tByte6\tByte7");
}


//fo show
void setup_blink() { // Function as Alarm or Find my eBike, It blinks the turning lights and blocks the motor
  unsigned long currentMillis = millis();
  unsigned long previousMillis = 0;
  Serialprint("blinking lights");
  digitalWrite(KEY, HIGH);  // ON POWER
  digitalWrite(IGNITION, HIGH); //we want to ensure the bike canNOT move
  digitalWrite(LIGHT, HIGH);
  Serialprint("blinking");
  delay(10000);
  digitalWrite(LIGHT, LOW);
  digitalWrite(KEY, LOW);  // OFF POWER
}

void setup_wifi() {
  Serialprint("System start.");
  //Serial.println("Modem: " + modem.getModemInfo());
  //Serial.println("Searching for telco provider.");
  if (!modem.waitForNetwork())
  {
    Serialprint("fail");
    while (true);
  }
  //Serial.println("Connected to telco.");
  //Serial.println("Signal Quality: " + String(modem.getSignalQuality()));
  //Serial.println("Connecting to GPRS network.");
  if (!modem.gprsConnect(apn, user, pass))
  {
    //    Serial.println("fail");
    while (true);
  }
  //Serial.println("Connected to GPRS: " + String(apn));
  client.setServer(mqtt_server, port);
  client.setCallback(callback);
  //Serial.print("Connecting to MQTT Broker: " + String(mqtt_server));
  while (reconnect() == false) continue;
}


boolean reconnect() {
  if (!client.connect("MQTTEcuClient", mqtt_user, mqtt_password))
  {
    Serialprint(".");
    return false;
  }
  Serialprint("Connected to broker.");
  subscribingMQTT();
  //client.subscribe(topicIn);
  return client.connected();
}

void subscribingMQTT() {
  client.subscribe("TestIn"); // for debugging
  client.subscribe("light/bool");
  client.subscribe("power/bool");
  client.subscribe("battery/call");
  client.subscribe("curstate/call");
}


//utility methods
bool isTopic(char* topic, char* publishTopic) {
  return (strcmp(topic, publishTopic) == 0);
}

char isPayload(byte* payload, char compchr) {
  return ((char)payload[0] == compchr);
}


void callback(char* topic, byte* payload, unsigned int length) {
  // go to CloudMQTT WEBSOCKET UI to send messages
  //Serial.print("Message arrived [");
  //Serial.print(topic);
  //Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.println((char)payload[i]);
  }

  if (isTopic(topic, "light/bool")) {
    if (isPayload(payload, '0')) {
      digitalWrite(LIGHT, LOW);   // OFF Turning Lights
      light = "false";
      client.publish("ecu/debug/light/status", light);
    } else if (isPayload(payload, '1')) {
      digitalWrite(LIGHT, HIGH);  // ON Turning Lights
      light = "true";
      client.publish("ecu/debug/light/status", light);
    }

  } else if (isTopic(topic, "power/bool")) {
    if (isPayload(payload, '0')) {
      digitalWrite(KEY, LOW);  // OFF POWER
      power = "false";
      client.publish("ecu/debug/power/status", power);
    } else if (isPayload(payload, '1')) {
      digitalWrite(KEY, HIGH);  // ON POWER
      digitalWrite(IGNITION, LOW); //we want to ensure the bike can move
      power = "true";
      client.publish("ecu/debug/power/status", power);
    }

  } else if (isTopic(topic, "TestIn")) { //DEBUG & Test
    if (isPayload(payload, 'b')) {
      batteryDebug();  // Read Battery Status
      //Serial.println("TestIn b - batt stat");
    } else if (isPayload(payload, 'k')) {
      setup_blink();  // for debug
      //Serial.println("TestIn k - blink");
    }

  } else if (isTopic(topic, "battery/call")) {
    client.publish("battery/json", "{\"id\":0,\"cycle\":\"85\",\"status\":\"46\" }");

  } else if (isTopic(topic, "curstate/call")) {
    /*
      dtostrf(GPS.latitude, 6, 2, latitude);
      dtostrf(GPS.longitude, 6, 2, longitude);
      dtostrf(GPS.altitude, 6, 2, altitude);
      dtostrf(battery, 2, 0, battstring);

      sprintf(data, "{\"id\":0,\"lat\":%s%s,\"lng\":%s%s,\"alt\":%s,\"bat\":%s}", &GPS.lat, latitude, &GPS.lon, longitude, altitude, battstring); //98 is a random num for batt %
    */
    client.publish("curstate/json", "data");

  } else {
    //errorMessage
    client.publish("inTopic/error", "error: topic doesn't match any callback topic");
  }
}





void batteryDebug() { //It reads the pin A0- connect the wire to 5V or 3.3V (will be High status)or GND(will be Low status)
  /*
  */
}


/*****************************************************************************/
/***************************    LOOP    **************************************/
/*****************************************************************************/
void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  //sendLocationStatus();
  sendDiagnostics();
  //sendBatteryStatus();
}

//loopy bits
void sendDiagnostics() {
  if (timerDiagnostics > millis())  timerDiagnostics = millis();
  if (millis() - timerDiagnostics > 2500) {
    timerDiagnostics = millis(); // reset the
    Serial.println("Diagnostics: ");
    //Serial.println("{\"id\":0,\"battery\":\"ok\",\"wheels\":\"ok\", \"lights\": [{\"front\": \"ok\"},{\"back\": \"ok\"},{\"other\": \"ok\"} ], \"suspention\": \"ok\", \"breaks\": \"ok\" }");
    //client.publish("Diagnostics","{\"id\":0,\"battery\":\"ok\",\"breaks\":\"ok\", \"lights\"[{\"front\": \"ok\"},{\"back\": \"ok\"},{\"suspention\": \"ok\"} ]");
    client.publish("diagnostics/breaks/string", breaks);
    client.publish("diagnostics/engine/string", engine);
    client.publish("diagnostics/ecu/string", ecu);
    client.publish("diagnostics/battery/string", batt);
  }
}

void sendLocationStatus() {

  //sendData("AT+CIPGSMLOC=1,1",3000,DEBUG);
  sendTabData("AT+CGNSINF", 1000, DEBUG);
  //sendData( "AT+CGNSINF",1000,DEBUG);   // returns a single NMEA sentence of GPS.
  if (state != 0) {
    //    Serial.println("State  :"+state);
    //    Serial.println("Time  :"+timegps);
    //    Serial.println("Latitude  :"+latitude);
    //    Serial.println("Longitude  :"+longitude);
    SerialAT.print(latitude);
    SerialAT.print(",");
    SerialAT.print (longitude);

    latitude.toCharArray(lat, 10);
    longitude.toCharArray(lon, 10);
    sprintf (coord, "{\"id\":0,\"longitude\":%s,\"latitude\":%s}", lon, lat);

    //   Serial.print("Publish message: ");
    //   Serial.println(coord);
    client.publish("gps/coordinates", coord);

    delay(200);
    SerialAT.println((char)26); // End AT command with a ^Z, ASCII code 26
    delay(5000);
    SerialAT.flush();
  } else {
    Serial.println("GPS Initialising...");
  }

}


void sendTabData(String command , const int timeout , boolean debug) {

  String data[5] = "";
  SerialAT.println(command);
  long int time = millis();
  int i = 0;

  while ((time + timeout) > millis()) {
    while (SerialAT.available()) {
      char c = SerialAT.read();
      if (c != ',') {
        data[i] += c;
        delay(100);
      } else {
        i++;
      }
      if (i == 5) {
        delay(100);
        goto exitL;
      }
    }
} exitL:
  if (debug) {
    state = data[1];
    timegps = data[2];
    latitude = data[3];
    longitude = data[4];
  }
}

void sendData(String command, const int timeout, boolean debug) {
  String response = "";
  SerialAT.println(command);
  delay(5);
  if (debug) {
    long int time = millis();
    while ( (time + timeout) > millis()) {
      while (SerialAT.available()) {
        response += char(SerialAT.read());
      }
    }
    // Serial.print(response);
  }
}

void sendBatteryStatus() {

  if ( power == "true") {
    Serialprint("Power is true");
    setup_can();
    if (CAN_MSGAVAIL == CAN.checkReceive())           // check if data coming
    {
      //Serial.print("received");
      rcvTime = millis();
      CAN.readMsgBuf(&len, buf);    // read data,  len: data length, buf: data buf
      rxId = CAN.getCanId();
      //Serial.print(rcvTime);
      //Serial.print("\t\t");
      //Serial.print("0x");
      //Serial.print(rxId);
      //Serial.print(rxId, HEX);
      //Serial.print("\t");
      for (byte i = 0; i < len; i++) // print the data
      {
        if (buf[i] > 8) {
          //Serial.print("0x");
          //Serial.print(buf[i], HEX);
        }
        else {
          //Serial.print("0x0");
          //Serial.print(buf[i], HEX);
        }

        //Serial.print("0x");
        //Serial.print(buf[i], HEX);

        //Serial.print("\t");
      }
      //Serial.println();
      if (rxId == 523) {
        bat = buf[2];
        //Serial.println("bat: ");
        //Serial.print(buf[2], HEX);
        //Serial.println(bat);
      }
      client.publish("diagnostics/battery/", bat);
    }
  } else {
    Serialprint("No CAN accesible");
  }

}


