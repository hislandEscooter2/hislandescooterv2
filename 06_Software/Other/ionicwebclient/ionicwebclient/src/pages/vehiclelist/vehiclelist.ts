import {Component, forwardRef, NgModule, OnInit} from '@angular/core';
import {IonicPage, NavController} from "ionic-angular";
import {Evehicle} from "./vehiclelist.model";
import {VehiclelistService} from "./vehiclelist.service";
import {HomePage} from "../home/home";
import {TabsPage} from "../tabs/tabs";

/**
 * Generated class for the VehiclelistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@NgModule({providers: [forwardRef(() => VehiclelistService)]})
  //@IonicPage()
@Component({
  selector: 'page-vehiclelist',
  templateUrl: 'vehiclelist.html',
})
export class VehiclelistPage implements OnInit{

  evehicles: Evehicle[];
  tabsPage = TabsPage;

  constructor(private vehiclelistService: VehiclelistService, public navCtrl: NavController) {
  }


  ngOnInit() {
    this.evehicles = this.vehiclelistService.getAllEVehicles();
  }

  moveToDetailPage() {
    this.navCtrl.push(this.tabsPage)
  }

  updateBatteryStatus(ev: Evehicle) {
    if (ev.batterystatus > 95) {
      return 'https://img.icons8.com/color/48/000000/full-battery.png';
    }
    if (ev.batterystatus > 75) {
      return 'https://img.icons8.com/color/48/000000/high-battery.png';
    }
    if (ev.batterystatus > 50) {
      return 'https://img.icons8.com/color/48/000000/medium-battery.png';
    }
    if (ev.batterystatus > 15) {
      return 'https://img.icons8.com/color/48/000000/low-battery.png';
    }
    if (ev.batterystatus > 4) {
      return 'https://img.icons8.com/color/48/000000/empty-battery.png';
    }
  }


}
