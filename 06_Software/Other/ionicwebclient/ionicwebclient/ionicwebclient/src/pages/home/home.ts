import {Component, ViewChild, ElementRef, NgZone, AfterViewInit} from '@angular/core';
import {IonicPage, Platform} from 'ionic-angular';
import {GoogleMap, Marker, LatLng} from "@ionic-native/google-maps";
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';

declare var google;

interface gpsDTOJSON {
  id: string;
  longitude: number;
  latitude: number;
}

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  //google maps properties
  @ViewChild('map_canvas') mapElement: ElementRef;
  location = new google.maps.LatLng(57.710518, 11.959543);
  marker: any;
  map: GoogleMap;
  latitude: any;
  longitude: any;
  id: any = "123";
  //websockets properties
  title = 'the ecu webclient';
  private serverUrl = 'http://localhost:9090/socket';
  private stompClient;

  ngAfterViewInit() {
    this.startMap();
  }

  constructor() {
    this.initializeWebSocketConnection();
  }


  startMap(): any {
    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 10,
      center: this.location,
      mapTypeId: 'roadmap'
    });

    this.marker = new google.maps.Marker({
      position: this.location,
      label: 'A',
      map: this.map
    });
    console.log(this.marker);

    /*    google.maps.event.addListener(this.map, 'click', (event) => {
          if (this.marker) {
            this.marker.setMap(null);
          }

          this.marker = this.addMarker(event.latLng, this.map, this.marker);
        });*/
    return this.map
  }


  initializeWebSocketConnection() {
    let ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe('/ecu', (message) => {
        console.log('message: ' + message.toString());
        console.log('body: ' + message.body);
        let obj: gpsDTOJSON = JSON.parse(message.body);
        that.latitude = obj.latitude;
        that.longitude = obj.longitude;
        that.updateOnMap(obj.latitude, obj.longitude);
      });
    });
  }

  updateOnMap(lat: any, lng: any) {
    if (this.marker) {
      this.marker.setMap(null);
    }
    this.marker = this.addMarker(
      new LatLng(lat, lng),
      this.map,
      this.marker);


  }

  private addMarker(location, map, marker): any {
    marker = new google.maps.Marker({
      position: location,
      label: 'A',
      map: map
    });
    return marker

  }
}
