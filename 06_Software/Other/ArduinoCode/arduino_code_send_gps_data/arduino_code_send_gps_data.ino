/*
 * SerialFormatting
 * Print values in various formats to the serial port
*/

#include <SoftwareSerial.h>

//#define ArduinoSerial Serial
SoftwareSerial ArduinoSerial(7, 8);


//todo how to encode a string to send it over serial? 
//const char* chrValue = "{\"id\":0,\"longitude\":57.710518,\"latitude\":11.947343}";
const char* chrValue = "banana";

//char chrValue  // these are the starting values to print
int intValue  = 65;
float floatValue = 65.0;
int i = 0;

void setup()
{
  pinMode(LED_BUILTIN,OUTPUT);
  //Serial.begin(9600);
  ArduinoSerial.begin(9600);

  
  
}

void loop()
{
  digitalWrite(LED_BUILTIN, HIGH);
  //todo split up the char*
  char sendBuffer[19];
  sprintf(sendBuffer, "57.710518 11.947343" );
  ArduinoSerial.println(sendBuffer); 
  //i++;
  delay(1000); // delay a second between numbers
  digitalWrite(LED_BUILTIN, LOW);
  delay(200);
}
