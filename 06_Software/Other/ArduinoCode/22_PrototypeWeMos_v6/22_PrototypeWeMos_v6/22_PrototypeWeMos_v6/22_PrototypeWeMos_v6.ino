/*


  Basic SIM808 MQTT example (GPS not developed yet)
  Able to turn on/off the eBike from cloudMQTT
  Boards used:
  Arduino UNO
  SIM808 shield from elecrow    for GPRS connection and GPS location

  This sketch demonstrates the capabilities of the pubsub library in combination
  with the TinyGSM - SIM808 board/library.
  It connects to an MQTT server then:
  - publishes "hello world" to the topic "outTopic" at the begining of the conexion
    check it at cloud mqtt "websocket ui"
    https://www.cloudmqtt.com/
  - subscribes to the topics for reading and publishing "TestIn", "light/bool","power/bool","battery/call" and "curstate/call"
    check the messages sent from Arduino and received also at cloud mqtt "websocket ui"
    It assumes the received payloads are strings not binary
  - Example. If the first character of the topic "light/bool" is an 1, switch ON the ESP Led
    check the actions that will occur depending of the Topic and its payload at 'callback()' function

  It will reconnect to the server if the connection is lost using a blocking
  reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
  achieve the same result without blocking the main loop.

  Library & Documentation API about PubSubClient() :
  https://pubsubclient.knolleary.net/api.html
  Library & Documentation API about TinyGSM() :
  https://github.com/vshymanskyy/TinyGSM/tree/master/src

  the WebApp is developed here: http://jgecu.surge.sh/
  Still need review in the BE&FE part.

  Code is a mix with PrototypeWemos_v5 and this web example: http://shahrulnizam.com/arduino-lesson-sim800-mqtt/


*/

/*------------- LIBRARIES Includes & Defines --------------*/
#define TINY_GSM_MODEM_SIM808
#include <TinyGsmClient.h>
#include <PubSubClient.h>
#include <SoftwareSerial.h>
SoftwareSerial SerialAT(7, 8); //RX,TX

/*------------- VARIABLES Declaration --------------*/
TinyGsm modem(SerialAT);
TinyGsmClient SIMclient(modem);
PubSubClient client(SIMclient); //id


//Network details
const char apn[]  = "online.telia.se";
const char user[] = "";
const char pass[] = "";


//mqtt vars
const char* mqtt_server = "m24.cloudmqtt.com" ;         // at CloudMQTT web :"Your MQTT Server IP address";
const char* mqtt_user = "rxhwkrwu" ;//"ovkfmmrv";  ovkfmmrv                   // Your user at CloudMQTT
const char* mqtt_password = "euP2FEqY5IlV";//"b6Q5wTu-3-OR";             //  Your password at CloudMQTT
const  int port =  14889;//15139;                                //just Port, not the SSL

//for control vars
int IGNITION = 2; //Power eBike on         //Switch RED - YELLOW
int LIGHT = 3 ;  //Power just Lights      //Switch RED - YELLOW  and PINK - AIR
int KEY = 4;      // for later
int T_LEFT = 5;   // Turning Light LEFT    //Switch GREY - ORANGE
int T_RIGHT = 6;  // Turning Light RIGHT   //Switch GREY - BLUE
int SIMPOWER = 9;

unsigned long previousMillis = 0;

//gps vars
char coord[40];
char data[20];
char latitude[10];
char longitude[10];
char altitude[10];

//batt vars
double battery = 90;
char battstring[2];

// sendLocationStatus and sendDiagnostic timers
uint32_t timer = millis();
uint32_t timerDiagnostics = millis();

//status and control vars of light and power
char* power = "false";
char* light = "false";

//diagnostic vars
char* breaks = "ok";
char* engine = "engine-paused health-ok";
char* ecu = "ecu-running-ok LiPo-bat-recharging";
char* batt = "f'healthd(0) battery l={batt_l} v={batt_v} t={batt_t} h=2 st=2'";


/*------------- FUNCTIONS Declaration --------------*/
void setup_wifi(void);
bool isTopic(char* topic, char* publishTopic);
char isPayload(byte* payload, char compchr);
void callback(char* topic, byte* payload, unsigned int length);
void errorMessage(void);
void batteryDebug(void);
void buzzer(void);
void setup_blink(void);
void sendLocationStatus(void);
void sendDiagnostics(void);
void subscribingMQTT();


/*--------------------- SET UP  ------------------------*/
void setup() {
  //setup_blink();
  Serial.begin(9600);
  SerialAT.begin(9600);
  pinMode(SIMPOWER, OUTPUT);
  digitalWrite(SIMPOWER, LOW);
  delay(1000);
  pinMode(IGNITION, OUTPUT);
  pinMode(LIGHT, OUTPUT);
  pinMode(KEY, OUTPUT);
  pinMode(T_LEFT, OUTPUT);
  pinMode(T_RIGHT, OUTPUT);
  //delay(5000);
  //Serial.setDebugOutput(true);
  setup_wifi();
  client.setServer(mqtt_server, port); // server, Port (not SSL)
  client.setCallback(callback);

  delay(1000);

}

//fo show
void setup_blink() { // Make all the LEDs blink to be sure the connections are good and debugg
  unsigned long currentMillis = millis();
  unsigned long previousMillis = 0;
  Serial.println("blinking lights");
  digitalWrite(IGNITION, HIGH);  // ON POWER
  //digitalWrite(KEY, HIGH); //we want to ensure the bike canNOT move
  digitalWrite(LIGHT, HIGH);
  Serial.print("blinking");
  delay(10000);
  digitalWrite(LIGHT, LOW);
  digitalWrite(IGNITION, LOW);  // OFF POWER
}

void buzzer() { // blinks the orange LED
  /*
     empty
  */
  Serial.println("Buzz Buzz");
}

void setup_wifi() {

  Serial.println("System start.");
  modem.restart();
  Serial.println("Modem: " + modem.getModemInfo());
  Serial.println("Searching for telco provider.");
  if (!modem.waitForNetwork())
  {
    Serial.println("fail");
    while (true);
  }
  Serial.println("Connected to telco.");
  Serial.println("Signal Quality: " + String(modem.getSignalQuality()));

  Serial.println("Connecting to GPRS network.");
  if (!modem.gprsConnect(apn, user, pass))
  {
    Serial.println("fail");
    while (true);
  }
  Serial.println("Connected to GPRS: " + String(apn));
  client.setServer(mqtt_server, port);
  client.setCallback(callback);
  Serial.println("Connecting to MQTT Broker: " + String(mqtt_server));
  while (reconnect() == false) continue;
  Serial.println();
}


boolean reconnect() {
  if (!client.connect("MQTTEcuClient", mqtt_user, mqtt_password))
  {
    Serial.print(".");
    return false;
  }
  Serial.println("Connected to broker.");
  subscribingMQTT();
  //client.subscribe(topicIn);
  return client.connected();
}

void subscribingMQTT() {
  client.subscribe("TestIn"); // for debugging
  client.subscribe("light/bool");
  client.subscribe("power/bool");
  client.subscribe("battery/call");
  client.subscribe("curstate/call");
}


//utility methods
bool isTopic(char* topic, char* publishTopic) {
  return (strcmp(topic, publishTopic) == 0);
}

char isPayload(byte* payload, char compchr) {
  return ((char)payload[0] == compchr);
}


void callback(char* topic, byte* payload, unsigned int length) {
  // go to CloudMQTT WEBSOCKET UI to send messages
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.println((char)payload[i]);
  }

  if (isTopic(topic, "light/bool")) {
    if (isPayload(payload, '0')) {
      digitalWrite(LIGHT, LOW);   // OFF LED
      light = "false";
      client.publish("ecu/debug/light/status", light);
    } else if (isPayload(payload, '1')) {
      digitalWrite(LIGHT, HIGH);  // ON LED
      light = "true";
      client.publish("ecu/debug/light/status", light);
    }

  } else if (isTopic(topic, "power/bool")) {
    if (isPayload(payload, '0')) {
      buzzer();
      digitalWrite(IGNITION, LOW);  // OFF POWER
      power = "false";
      client.publish("ecu/debug/power/status", power);
    } else if (isPayload(payload, '1')) {
      digitalWrite(IGNITION, HIGH);  // ON POWER
      digitalWrite(KEY, LOW); //we want to ensure the bike can move
      power = "true";
      client.publish("ecu/debug/power/status", power);
    }

  } else if (isTopic(topic, "TestIn")) { //DEBUG & Test
    if (isPayload(payload, 'b')) {
      batteryDebug();  // Read Battery Status
      Serial.println("TestIn b - batt stat");
    } else if (isPayload(payload, 'k')) {
      setup_blink();  // for debug
      Serial.println("TestIn k - blink");
    }

  } else if (isTopic(topic, "battery/call")) {
    client.publish("battery/json", "{\"id\":0,\"cycle\":\"85\",\"status\":\"46\" }");

  } else if (isTopic(topic, "curstate/call")) {
    /*
      dtostrf(GPS.latitude, 6, 2, latitude);
      dtostrf(GPS.longitude, 6, 2, longitude);
      dtostrf(GPS.altitude, 6, 2, altitude);
      dtostrf(battery, 2, 0, battstring);

      sprintf(data, "{\"id\":0,\"lat\":%s%s,\"lng\":%s%s,\"alt\":%s,\"bat\":%s}", &GPS.lat, latitude, &GPS.lon, longitude, altitude, battstring); //98 is a random num for batt %
    */
    client.publish("curstate/json", "data");

  } else {
    //errorMessage
    client.publish("inTopic/error", "error: topic doesn't match any callback topic");
  }
}





void batteryDebug() { //It reads the pin A0- connect the wire to 5V or 3.3V (will be High status)or GND(will be Low status)
  /*
    int batValue = 0;
    batValue = analogRead(BAT);
    digitalWrite(BAT_GR, HIGH);
    digitalWrite(BAT_RED, HIGH);
    if ( batValue < 20 ) {
    Serial.print("Publish message: ");
    Serial.println(batValue);
    client.publish("debug/batteryDebug", "Low");
    digitalWrite(BAT_RED, LOW);
    } else if ( batValue > 150 ) {
    Serial.print("Publish message: ");
    Serial.println(batValue);
    client.publish("debug/batteryDebug", "High");
    digitalWrite(BAT_GR, LOW);
    } else {
    client.publish("debug/batteryDebug", "Normal");
    digitalWrite(BAT_GR, HIGH);
    digitalWrite(BAT_RED, HIGH);
    }
  */
}


/*****************************************************************************/
/***************************    LOOP    **************************************/
/*****************************************************************************/
void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  sendLocationStatus();
  sendDiagnostics();
  sendBatteryStatus();
}

//loopy bits
void sendDiagnostics() {
  if (timerDiagnostics > millis())  timerDiagnostics = millis();
  if (millis() - timerDiagnostics > 2500) {
    timerDiagnostics = millis(); // reset the
    Serial.println("Diagnostics: ");
    //Serial.println("{\"id\":0,\"battery\":\"ok\",\"wheels\":\"ok\", \"lights\": [{\"front\": \"ok\"},{\"back\": \"ok\"},{\"other\": \"ok\"} ], \"suspention\": \"ok\", \"breaks\": \"ok\" }");
    //client.publish("Diagnostics","{\"id\":0,\"battery\":\"ok\",\"breaks\":\"ok\", \"lights\"[{\"front\": \"ok\"},{\"back\": \"ok\"},{\"suspention\": \"ok\"} ]");
    client.publish("diagnostics/breaks/string", breaks);
    client.publish("diagnostics/engine/string", engine);
    client.publish("diagnostics/ecu/string", ecu);
    client.publish("diagnostics/battery/string", batt);
  }
}

void sendLocationStatus() {
  /*
    char c = GPS.read();
    if ((c) && (GPSECHO))
    Serial.write(c);
    if (GPS.newNMEAreceived()) {
    if (!GPS.parse(GPS.lastNMEA()))
      return;
    }
    if (timer > millis())  timer = millis();
    if (millis() - timer > 2000) {
    timer = millis(); // reset the timer
    if (GPS.fix) {
      Serial.print("Location: ");
      Serial.print(GPS.latitude, 4); Serial.print(GPS.lat);
      Serial.print(", ");
      Serial.print(GPS.longitude, 4); Serial.println(GPS.lon);

      Serial.print("Speed (knots): "); Serial.println(GPS.speed);
      Serial.print("Angle: "); Serial.println(GPS.angle);
      Serial.print("Altitude: "); Serial.println(GPS.altitude);
      Serial.print("Satellites: "); Serial.println((int)GPS.satellites);
    }

    dtostrf(GPS.latitude, 6, 2, latitude);
    dtostrf(GPS.longitude, 6, 2, longitude);
    sprintf (coord, "{\"id\":0,\"longitude\":%s,\"latitude\":%s}", latitude, longitude);
    Serial.print("Publish message: ");
    Serial.println(coord);
    client.publish("gps/coordinates", coord);
    }
  */
}


void sendBatteryStatus() {
  if (timer > millis())  timer = millis();
  if (millis() - timer > 2000) {
    timer = millis(); // reset the timer
    dtostrf(battery, 2, 0, battstring);
    client.publish("battery/json", battstring);
    battery = battery - 1;
  }
}


