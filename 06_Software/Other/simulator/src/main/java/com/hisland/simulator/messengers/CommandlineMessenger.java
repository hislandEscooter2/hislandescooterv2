package com.hisland.simulator.messengers;


import com.hisland.simulator.model.GPSCoordinate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(name = "messenger.type", havingValue = "command")
public class CommandlineMessenger implements Messenger {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandlineMessenger.class);


    public CommandlineMessenger() {
    }

    public void sendMessage(GPSCoordinate message) {
        LOGGER.info("CommandlineMessenger sent message");
        System.out.println(message);

    }
}
