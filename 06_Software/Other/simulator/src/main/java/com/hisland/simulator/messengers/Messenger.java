package com.hisland.simulator.messengers;

import com.hisland.simulator.model.GPSCoordinate;
import org.springframework.stereotype.Component;

@Component
public interface Messenger {
    void sendMessage(GPSCoordinate message);
}
