package com.hisland.simulator.converters;

import com.hisland.simulator.model.GPSCoordinate;

import javax.xml.bind.JAXBException;

public interface Converter {
    String convert(GPSCoordinate gpsCoordinate) throws JAXBException;
}
