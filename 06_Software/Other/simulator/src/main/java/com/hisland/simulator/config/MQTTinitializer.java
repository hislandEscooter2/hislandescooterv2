package com.hisland.simulator.config;

import com.hisland.simulator.messengers.QueueMessenger;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.UUID;

@Component
public class MQTTinitializer {
    private static final Logger LOGGER = LoggerFactory.getLogger(QueueMessenger.class);

    private IMqttClient mqttClient;
    MqttConnectOptions mqttConnectOptions;

    public MQTTinitializer() {
    }

    public IMqttClient init(MQTTConfig configuration) {
        //int retryInterval = configuration.getRetryInterval();

        mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setCleanSession(false);
        mqttConnectOptions.setAutomaticReconnect(true);
        String clientId = configuration.getClientId();
        if (StringUtils.isEmpty(clientId)) {
            clientId = UUID.randomUUID().toString();
        }
        if (!StringUtils.isEmpty(configuration.getAccessToken())) {
            //mqttConnectOptions.setUserName(configuration.getAccessToken());
        }
        mqttConnectOptions.setUserName(configuration.getUsername());
        try {
            mqttClient = new MqttClient("tcp://" + configuration.getHost() + ":" + configuration.getPort(), clientId);
            mqttConnectOptions.setPassword(configuration.getPassword().toCharArray());
            mqttClient.connect(mqttConnectOptions);
        } catch (Exception e) {
            LOGGER.error("Failed to create mqtt client", e);
            throw new RuntimeException(e);
        }
        // connect();
        return mqttClient;
    }
}
