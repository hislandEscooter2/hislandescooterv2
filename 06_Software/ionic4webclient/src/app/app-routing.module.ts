import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './auth/auth.guard';

const routes: Routes = [

    {path: '', redirectTo: 'auth', pathMatch: 'full'},
    {path: 'controlcenter', loadChildren: './tabs/tabs.module#TabsPageModule'/*, canLoad: [AuthGuard]*/},
    {path: 'auth', loadChildren: './auth/auth.module#AuthPageModule'}
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
