import { Injectable } from '@angular/core';
import { Diagnostic } from './diagnostics.model';

@Injectable({
    providedIn: 'root'
})
export class DiagnosticsService {
    private diagnostics: Diagnostic[] = [
        new Diagnostic("Battery"),
        new Diagnostic("ECU"),
        new Diagnostic("Breaks"),
        new Diagnostic("Engine"),
    ]
    constructor() { }
    getDiagnosticDetails() {
        return [...this.diagnostics]
    }
    getDiagnosticDetail(id: string) {
        return { ...this.diagnostics.find(d => d.name === id) };
    }
}

