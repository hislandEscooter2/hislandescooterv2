import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
    {
        path: 'tabs',
        component: TabsPage,
        children: [
            
            {
                path: 'home', children: [
                    {
                        path: '', loadChildren: './home/home.module#HomePageModule'
                    }
                ]
            },
            {
                path: 'map', children: [
                    {
                        path: '', loadChildren: './map/map.module#MapPageModule'
                    }
                ]
            },
            {
                path: 'controls', children: [
                    {
                        path: '', loadChildren: './controls/controls.module#ControlsPageModule'
                    }
                ]
            },
            {
                path: 'diagnostics', children: [
                    {
                        path: '', loadChildren: './diagnostics/diagnostics.module#DiagnosticsPageModule'
                    },
                    {
                        path: ':diagnosticsId',
                        loadChildren: './diagnostics/diagnosticsdetail/diagnosticsdetail.module#DiagnosticsdetailPageModule'
                    }
                ]
            },
            {
                path: 'details', children: [
                    {
                        path: '', loadChildren: './details/details.module#DetailsPageModule'
                    }
                ]
            },
            {
                path: 'user', children: [
                    {
                        path: '', loadChildren: './usersettings/usersettings.module#UsersettingsPageModule'
                    }
                ]
            },
            {
                path: '',
                redirectTo: 'controlcenter/tabs/map',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: 'controlcenter/tabs/map',
        pathMatch: 'full'
    },
    { path: 'map', loadChildren: './map/map.module#MapPageModule' }


];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TabsRoutingModule {

}
