import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/internal/operators/tap';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    _userIsAuthenticated = false;

    get userIsAuthenticated() {
        return this._userIsAuthenticated;
    }

    constructor() {
    }

    login(form) {
        this._userIsAuthenticated = true;
    }

 /*   login(form:any): Observable<AuthResponse> {
        return this.httpClient.post(`${this.AUTH_SERVER_ADDRESS}/login`, user).pipe(
            tap(async (res: AuthResponse) => {

                if (res.user) {
                    await this.storage.set("ACCESS_TOKEN", res.user.access_token);
                    await this.storage.set("EXPIRES_IN", res.user.expires_in);
                    this.authSubject.next(true);
                }
            })
        );
    }
*/
    logout() {
        this._userIsAuthenticated = false;
    }
}
