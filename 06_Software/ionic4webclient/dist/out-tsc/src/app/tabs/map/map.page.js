import * as tslib_1 from "tslib";
import { Component, ViewChild, ElementRef } from '@angular/core';
import { LatLng } from '@ionic-native/google-maps';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
var MapPage = /** @class */ (function () {
    function MapPage() {
        this.location = new google.maps.LatLng(57.710518, 11.959543);
        this.id = '123';
        // websockets properties
        this.title = 'the ecu webclient';
        this.serverUrl = 'http://localhost:9090/socket';
        this.initializeWebSocketConnection();
    }
    MapPage.prototype.ngAfterViewInit = function () {
        this.startMap();
    };
    MapPage.prototype.startMap = function () {
        this.map = new google.maps.Map(this.mapElement.nativeElement, {
            zoom: 10,
            center: this.location,
            mapTypeId: 'roadmap'
        });
        this.marker = new google.maps.Marker({
            position: this.location,
            label: 'A',
            map: this.map
        });
        console.log(this.marker);
        /*    google.maps.event.addListener(this.map, 'click', (event) => {
              if (this.marker) {
                this.marker.setMap(null);
              }

              this.marker = this.addMarker(event.latLng, this.map, this.marker);
            });*/
        return this.map;
    };
    MapPage.prototype.initializeWebSocketConnection = function () {
        var ws = new SockJS(this.serverUrl);
        this.stompClient = Stomp.over(ws);
        var that = this;
        this.stompClient.connect({}, function () {
            that.stompClient.subscribe('/Gps', function (message) {
                console.log('message: ' + message.toString());
                console.log('body: ' + message.body);
                var obj = JSON.parse(message.body);
                that.latitude = obj.latitude;
                that.longitude = obj.longitude;
                that.updateOnMap(obj.latitude, obj.longitude);
            });
        });
    };
    MapPage.prototype.updateOnMap = function (lat, lng) {
        if (this.marker) {
            this.marker.setMap(null);
        }
        this.marker = this.addMarker(new LatLng(lat, lng), this.map, this.marker);
    };
    MapPage.prototype.addMarker = function (location, map, marker) {
        marker = new google.maps.Marker({
            position: location,
            label: 'A',
            map: map
        });
        return marker;
    };
    tslib_1.__decorate([
        ViewChild('map_canvas'),
        tslib_1.__metadata("design:type", ElementRef)
    ], MapPage.prototype, "mapElement", void 0);
    MapPage = tslib_1.__decorate([
        Component({
            selector: 'app-map',
            templateUrl: './map.page.html',
            styleUrls: ['./map.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], MapPage);
    return MapPage;
}());
export { MapPage };
//# sourceMappingURL=map.page.js.map