import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ControlsPage } from './controls.page';
var routes = [
    {
        path: '',
        component: ControlsPage
    }
];
var ControlsPageModule = /** @class */ (function () {
    function ControlsPageModule() {
    }
    ControlsPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [ControlsPage]
        })
    ], ControlsPageModule);
    return ControlsPageModule;
}());
export { ControlsPageModule };
//# sourceMappingURL=controls.module.js.map