import * as tslib_1 from "tslib";
import { Component } from "@angular/core";
import { AlertController, LoadingController } from "@ionic/angular";
import { HttpClient, HttpHeaders } from "@angular/common/http";
var ControlsPage = /** @class */ (function () {
    function ControlsPage(alertCtrl, http, loadingController) {
        this.alertCtrl = alertCtrl;
        this.http = http;
        this.loadingController = loadingController;
    }
    ControlsPage.prototype.ngOnInit = function () {
        // TODO: here you have to add a rest call to know the status of the power via services and then inject
        this.isPowered = false;
    };
    ControlsPage.prototype.togglePower = function () {
        this.presentLoading();
        var _this = this;
        this.isPowered = !this.isPowered;
        var httpOptions = {
            headers: new HttpHeaders({
                "Content-Type": "application/json"
            })
        };
        // this.http.post<String>('http://localhost:9090/postLight', '0', httpOptions ).subscribe(
        //   data => {
        //     alert('ok');
        //   }
        // )
        if (this.isPowered) {
            this.isPowered = false;
            console.log("on");
            this.http
                .post("http://localhost:9090/postLight", "1", httpOptions)
                .subscribe(function (data) {
                alert("ok");
            });
        }
        else {
            console.log("off");
            this.isPowered = true;
            this.http
                .post("http://localhost:9090/postLight", "0", httpOptions)
                .subscribe(function (data) {
                alert("ok");
            });
        }
    };
    ControlsPage.prototype.toggleLight = function () {
        this.presentLoading();
        var _this = this;
        this.isPowered = !this.isPowered;
        var httpOptions = {
            headers: new HttpHeaders({
                "Content-Type": "application/json"
            })
        };
        // this.http.post<String>('http://localhost:9090/postLight', '0', httpOptions ).subscribe(
        //   data => {
        //     alert('ok');
        //   }
        // )
        if (this.isPowered) {
            this.isPowered = false;
            console.log("on");
            this.http
                .post("http://localhost:9090/postLight", "1", httpOptions)
                .subscribe(function (data) {
                alert("ok");
            });
        }
        else {
            console.log("off");
            this.isPowered = true;
            this.http
                .post("http://localhost:9090/postLight", "0", httpOptions)
                .subscribe(function (data) {
                alert("ok");
            });
        }
    };
    ControlsPage.prototype.presentLoading = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading, _a, role, data;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.loadingController.create({
                            message: 'Please wait',
                            duration: 1000
                        })];
                    case 1:
                        loading = _b.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _b.sent();
                        return [4 /*yield*/, loading.onDidDismiss()];
                    case 3:
                        _a = _b.sent(), role = _a.role, data = _a.data;
                        console.log('Loading dismissed!');
                        return [2 /*return*/];
                }
            });
        });
    };
    ControlsPage = tslib_1.__decorate([
        Component({
            selector: "app-controls",
            templateUrl: "./controls.page.html",
            styleUrls: ["./controls.page.scss"]
        }),
        tslib_1.__metadata("design:paramtypes", [AlertController, HttpClient,
            LoadingController])
    ], ControlsPage);
    return ControlsPage;
}());
export { ControlsPage };
//# sourceMappingURL=controls.page.js.map