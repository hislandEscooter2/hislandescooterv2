import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
var TabsService = /** @class */ (function () {
    function TabsService(http) {
        this.http = http;
        this.httpOptions = {
            headers: new HttpHeaders({
                "Content-Type": "application/json"
            })
        };
    }
    TabsService.prototype.getVehiclesStatus = function () {
        var _this = this;
        this.http
            .get("http://localhost:9090/light", this.httpOptions)
            .subscribe(function (data) {
            console.log("yup");
            (function (lightData) { return _this._light = lightData.Content; });
            (function (lightData) { return console.log("data    " + lightData.Content); });
        }).add(console.log("rtyutrtyutr  " + this._light));
        /*  this.http
          .get<String>("http://localhost:9090/power", this.httpOptions)
          .subscribe(data => {
            powerData => this._power = powerData
          });
        this.http
          .get<String>("http://localhost:9090/batterystatus", this.httpOptions)
          .subscribe(data => {
            batteryStatusData => this._batteryStatus = batteryStatusData
          });*/
    };
    TabsService.prototype.light = function () {
        return this._light;
    };
    TabsService.prototype.power = function () {
        return this._power;
    };
    TabsService.prototype.batteryStatus = function () {
        return this._batteryStatus;
    };
    TabsService.prototype.setLight = function (light) {
        this._light = light;
    };
    TabsService.prototype.setPower = function (power) {
        this._power = power;
    };
    TabsService.prototype.setBatteryStatus = function (batteryStatus) {
        this._batteryStatus = batteryStatus;
    };
    TabsService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], TabsService);
    return TabsService;
}());
export { TabsService };
//# sourceMappingURL=tabs.service.js.map