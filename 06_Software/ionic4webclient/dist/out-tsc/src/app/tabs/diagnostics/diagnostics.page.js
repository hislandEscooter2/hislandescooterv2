import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var DiagnosticsPage = /** @class */ (function () {
    function DiagnosticsPage() {
    }
    DiagnosticsPage.prototype.ngOnInit = function () {
    };
    DiagnosticsPage = tslib_1.__decorate([
        Component({
            selector: 'app-diagnostics',
            templateUrl: './diagnostics.page.html',
            styleUrls: ['./diagnostics.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], DiagnosticsPage);
    return DiagnosticsPage;
}());
export { DiagnosticsPage };
//# sourceMappingURL=diagnostics.page.js.map