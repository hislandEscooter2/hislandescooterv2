package com.hisland.backend.communication.processing;

import com.hisland.backend.BackendApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BackendApplication.class)
public class LightProcessorTest {
    @Mock
    LightProcessor lightProcessor;

    @Before
    public void setup() {
        when(lightProcessor.getLight()).thenReturn(true);
    }


    @Test
    public void detect() {
        lightProcessor.detect("test");
        verify(lightProcessor   , times(1)).detect("test");
    }

    @Test
    public void getLight() {
        lightProcessor.detect("true");
        assertEquals(lightProcessor.getLight() , true);
    }

    @Test
    public void getLight2() {
        lightProcessor.detect("true");
        assertNotEquals(lightProcessor.getLight() , false);
    }


}
