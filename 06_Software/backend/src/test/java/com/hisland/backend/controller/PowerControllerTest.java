package com.hisland.backend.controller;

import com.hisland.backend.BackendApplication;
import com.hisland.backend.service.ProcessService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.*;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BackendApplication.class)
public class PowerControllerTest {

    @Autowired
    private WebApplicationContext wac;
    @MockBean
    ProcessService processService;
    private MockMvc mockMvc;
    private static final String CONTENT_TYPE = "application/x-www-form-urlencoded";


    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();

    }

    @Test
    public void successPowerCall() throws Exception {
        mockMvc.perform(post("/power")
                .contentType(CONTENT_TYPE)
                .content("0")
                .accept(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void fail1PowerCall() throws Exception {
        mockMvc.perform(post("/powers")
                .contentType(CONTENT_TYPE)
                .content("0")
                .accept(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
    @Test
    public void fail2PowerCall() throws Exception {
        mockMvc.perform(post("/power")
                .contentType(CONTENT_TYPE)
                .content("3")
                .accept(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
    @Test
    public void fail3PowerCall() throws Exception {
        mockMvc.perform(post("/power")
                .contentType(CONTENT_TYPE)
                .content("sss")
                .accept(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}
