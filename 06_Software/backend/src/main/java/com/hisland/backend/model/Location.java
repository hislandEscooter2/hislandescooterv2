package com.hisland.backend.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String address;
    private String city;
    private String Country;

    @OneToMany(targetEntity = GPSCoordinate.class,
            cascade = {CascadeType.REMOVE})
    List<GPSCoordinate> gpsCoordinates;

    public Location(String address, String city, String country) {
        this.address = address;
        this.city = city;
        Country = country;
    }

    public Location() {
    }

    public List<GPSCoordinate> getGpsCoordinates() {
        return gpsCoordinates;
    }

    public void setGpsCoordinates(List<GPSCoordinate> gpsCoordinates) {
        this.gpsCoordinates = gpsCoordinates;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }
}
