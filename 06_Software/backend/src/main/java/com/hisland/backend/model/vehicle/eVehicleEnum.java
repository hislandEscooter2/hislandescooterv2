package com.hisland.backend.model.vehicle;

public enum eVehicleEnum {
    EBIKE(0), ESCOOTER(1);


    private int i;
    eVehicleEnum(int i) {
        this.i=i;
    }

    public int getI() {
        return i;
    }
}
