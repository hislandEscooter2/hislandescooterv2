package com.hisland.backend.config;

import com.hisland.backend.OnStart;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OnStartConfiguration {

    @Bean
    public OnStart onStart() {
        return new OnStart();
    }
}
