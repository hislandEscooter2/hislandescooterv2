package com.hisland.backend.model;

public class MatrixData {
    private String status;
    private Duration DurationObject;
    private Distance DistanceObject;
    private Fare FareObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Duration getDuration() {
        return DurationObject;
    }

    public Distance getDistance() {
        return DistanceObject;
    }

    public Fare getFare() {
        return FareObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDuration(Duration durationObject) {
        this.DurationObject = durationObject;
    }

    public void setDistance(Distance distanceObject) {
        this.DistanceObject = distanceObject;
    }

    public void setFare(Fare fareObject) {
        this.FareObject = fareObject;
    }
}
