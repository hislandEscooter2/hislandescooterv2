package com.hisland.backend.config;

import com.hisland.backend.communication.receiver.MQTTReceiver;
import org.eclipse.paho.client.mqttv3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * using the MQTTConfig we initialize the connection to the broker
 */
@Component
public class MQTTInitializer {
    private static final Logger LOGGER = LoggerFactory.getLogger(MQTTInitializer.class);

    private IMqttClient mqttClient;

    private final MQTTConfig config;

    @Autowired
    public MQTTInitializer( MQTTConfig config) {
        this.config = config;
    }


    /**
     * needs to be ran before we communicate with the broker
     * @return
     */
    public IMqttClient init() {
        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setCleanSession(true);
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setPassword(config.getPassword().toCharArray());
        mqttConnectOptions.setUserName(config.getUsername());



        try {
            mqttClient = new MqttClient(config.getPortProtocol() + "://" + config.getHost() + ":" + config.getPort(), config.getClientId());
            mqttClient.connect(mqttConnectOptions);
        } catch (MqttException e) {
            LOGGER.error(e.toString());
            LOGGER.error(this.getClass() + ": Failed to connect mqtt client", e);
        }

        return mqttClient;
    }
}
