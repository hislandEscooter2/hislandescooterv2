package com.hisland.backend.communication.processing;

import com.hisland.backend.communication.Notifier;
import com.hisland.backend.exception.CommunicationException;
import com.hisland.backend.exception.MyException;
import com.hisland.backend.service.ConvertService;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

/*assuming you get data like this*/
/*{
  "status": "OK",
  "duration": {
  "value": "320190",
  "text": "3 hours 22 minutes"
  },
  "distance": {
    "value": "1734542",
    "text": "1 735 km"
  },
  "fare" : {
    "currency" : "EUR",
    "value" : "6",
    "text" : "€6.00"
  },
}*/
@Component("distance/json")
public class DistanceProcessor extends Notifier implements Processor{
    private static final Logger LOGGER = LoggerFactory.getLogger(DistanceProcessor.class);
    private final
    SimpMessagingTemplate template;

    ConvertService convertService;
    private ModelMapper modelMapper;

    public DistanceProcessor(SimpMessagingTemplate template) {
        this.template = template;
    }


    public boolean calculate()throws MyException{
        //as it is not possible to make the calculation methods in the time we have left in this internship...
        //I will try to make a rough calculation in comments to have something to start with...
        //the architecture is already usable... its just the execution and the development of the methods that need work

        //calculation
        //bike has a total capacity of 26AH
        //standard power consumption 1.7KW/h
        //Rated voltage 60V
        //////
        //formula: Ampere hour = Watt hour / Voltage
        //1kW = 1000W
        return false;
    }

    //this should convert to pojo... does not work but should be something like this
    //so the void should change to MatrixData
    private void convertToData(String MQTTMessageData){
         modelMapper.addConverter(MappingContext::getDestination);
    }

    @Override
    public void detect(String MQTTMessageData) throws CommunicationException {
        LOGGER.info("DistanceProcessor IS ACTIVATED");
        convertToData(MQTTMessageData);
        template.convertAndSend("/distanceProcessor", MQTTMessageData);
    }
}
