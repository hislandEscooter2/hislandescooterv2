package com.hisland.backend.controller;

import com.hisland.backend.communication.processing.LightProcessor;
import com.hisland.backend.exception.CommunicationException;
import com.hisland.backend.service.ProcessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Controller
public class LightController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LightController.class);
    private final ProcessService processService;
    private final LightProcessor lightProcessor;



    @Autowired
    public LightController(ProcessService processService, LightProcessor lightProcessor) {
        this.processService = processService;
        this.lightProcessor = lightProcessor;
    }


    @PostMapping("/light")
   // @ResponseStatus(HttpStatus.OK)
    public ResponseEntity lightsCall(@RequestBody String bool) throws CommunicationException {
        LOGGER.info("light call" + bool);
        if (bool.toLowerCase().equals("0")||bool.toLowerCase().equals("1")){
            processService.setLightProcessor(bool);
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }


    @GetMapping("/light")
    public ResponseEntity<String> getLight() {
        LOGGER.info("light call");
        return new ResponseEntity(String.valueOf(lightProcessor.getLight()), HttpStatus.OK);
    }

}
