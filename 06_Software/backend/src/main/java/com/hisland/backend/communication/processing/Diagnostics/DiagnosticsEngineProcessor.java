package com.hisland.backend.communication.processing.Diagnostics;

import com.hisland.backend.communication.processing.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component("diagnostics/engine/string")
public class DiagnosticsEngineProcessor implements Processor {
    private static final Logger LOGGER = LoggerFactory.getLogger(DiagnosticsEngineProcessor.class);

    private final
    SimpMessagingTemplate template;

    @Autowired
    public DiagnosticsEngineProcessor(SimpMessagingTemplate template) {
        this.template = template;
    }

    @Override
    public void detect(String MQTTMessageData) {
        LOGGER.info("before webs send: " + MQTTMessageData);
        template.convertAndSend("/Engine",  MQTTMessageData);
    }
}
