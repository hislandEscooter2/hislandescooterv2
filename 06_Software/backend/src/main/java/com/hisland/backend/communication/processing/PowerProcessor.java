package com.hisland.backend.communication.processing;

import com.hisland.backend.communication.Notifier;
import com.hisland.backend.communication.receiver.MQTTReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

/**
 * processes power data
 */
@Component("power/bool")
public class PowerProcessor extends Notifier implements Processor {
    private static final Logger LOGGER = LoggerFactory.getLogger(PowerProcessor.class);

    private final
    SimpMessagingTemplate template;

    private
    boolean power;



    @Autowired
    public PowerProcessor(SimpMessagingTemplate template) {
        this.template = template;

    }

    /**
     * changes state of power boolean
     * @param MQTTMessageData
     */
    @Override
    public void detect(String MQTTMessageData) {
        LOGGER.info("PowerProcessor IS ACTIVATED");

        if (MQTTMessageData.toLowerCase().equals("true")){
            power = true;
        }else {
            power = false;
        }

    }

    /**
     * returns power data
     * @return
     */
    public boolean getPower() {
        return power;
    }
}
