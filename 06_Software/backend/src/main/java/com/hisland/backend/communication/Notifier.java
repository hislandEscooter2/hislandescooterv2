package com.hisland.backend.communication;


import com.hisland.backend.communication.processing.Processor;
import com.hisland.backend.exception.CommunicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This class is responsible for
 * - notifying the listeners
 * - triggering the retry
 */
@Component
public class Notifier {
    private static final Logger LOGGER = LoggerFactory.getLogger(Notifier.class);

    private
    BeanFactory bf;

    @Autowired
    public Notifier(BeanFactory bf) {
        this.bf = bf;
    }

    public Notifier() {
    }

    /**
     * we trigger the correct processor in this method
     * the topic is the same name as the spring annotation so when the message is received the right processor gets triggered
     * @param JSONMessageGPSData
     * @param topic
     */
    public void notifyListeners(String JSONMessageGPSData, String topic)  {
        try {
            bf.getBean(topic, Processor.class).detect(JSONMessageGPSData);
        } catch (CommunicationException e) {
            LOGGER.error(e.toString());
        }
    }

}
