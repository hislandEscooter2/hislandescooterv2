package com.hisland.backend.communication.processing.Diagnostics;

import com.hisland.backend.communication.processing.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component("diagnostics/ecu/string")
public class DiagnosticsECUProcessor implements Processor {
    private static final Logger LOGGER = LoggerFactory.getLogger(DiagnosticsECUProcessor.class);

    private final
    SimpMessagingTemplate template;

    @Autowired
    public DiagnosticsECUProcessor(SimpMessagingTemplate template) {
        this.template = template;
    }

    @Override
    public void detect(String MQTTMessageData) {
        LOGGER.info("before webs send: " + MQTTMessageData);
        template.convertAndSend("/ECU",  MQTTMessageData);
    }
}
