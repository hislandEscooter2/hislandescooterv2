package com.hisland.backend.controller;

import com.hisland.backend.service.ProcessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Controller
public class DiagnosticsController {
    private static final Logger LOGGER = LoggerFactory.getLogger(DiagnosticsController.class);

    private final
    ProcessService processService;

    public DiagnosticsController(ProcessService processService) {
        this.processService = processService;
    }

    @PostMapping("/diagnostics")
    public void diagnosticsCall(@RequestBody String bool) {
        LOGGER.info("diagnostics call");
    }


}
