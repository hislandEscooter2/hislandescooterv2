package com.hisland.backend.converter;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.hisland.backend.model.GPSCoordinate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * This class has a start that will
 * convert an XML massageString into a GPSCoordinate Object and will return it.
 */
@Component
public class ObjectConverter {
    private static final Logger LOGGER = LoggerFactory.getLogger(ObjectConverter.class);

    private ObjectMapper objectMapper;


    public GPSCoordinate convertXml(String JSONString) throws IOException {
        GPSCoordinate gPSCoordinate;

        gPSCoordinate = objectMapper.readValue(JSONString, GPSCoordinate.class);
        return gPSCoordinate;

    }
}
