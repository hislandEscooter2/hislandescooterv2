package com.hisland.backend.service;

import com.hisland.backend.communication.processing.BatteryProcessor;
import com.hisland.backend.communication.processing.Diagnostics.DiagnosticsBatteryProcessor;
import com.hisland.backend.communication.processing.LightProcessor;
import com.hisland.backend.communication.processing.PowerProcessor;
import com.hisland.backend.communication.publisher.MQTTPublisher;
import com.hisland.backend.exception.CommunicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ProcessService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessService.class);



    private final LightProcessor lightProcessor;
    private final PowerProcessor powerProcessor;
    private final MQTTPublisher mqttPublisher;

    private Map<String, String> topicNames = new HashMap<>();

    @Autowired
    public ProcessService(LightProcessor lightProcessor, PowerProcessor powerProcessor, MQTTPublisher mqttPublisher) {
        this.lightProcessor = lightProcessor;
        this.powerProcessor = powerProcessor;
        this.mqttPublisher = mqttPublisher;
        topicNames.put("light", "light/bool");
        topicNames.put("gps", "gps/coordinates");
        topicNames.put("power", "power/bool");
        topicNames.put("battery", "battery/call");
    }

    public void setLightProcessor(String bool) throws CommunicationException {
        LOGGER.info("setLightProcessor");
        lightProcessor.detect(bool);
        mqttPublisher.publish(bool, topicNames.get("light"));
    }

    public void setPowerProcessor(String bool) throws CommunicationException {
        LOGGER.info("setPowerProcessor");
        powerProcessor.detect(bool);
        mqttPublisher.publish(bool, topicNames.get("power"));
    }

    public void setBatteryProcessor() throws CommunicationException {
        mqttPublisher.publish("", topicNames.get("battery"));
    }
}
