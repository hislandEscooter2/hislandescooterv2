package com.hisland.backend.exception;

public class MyException extends Exception {
    public MyException(String message, Throwable cause) {
        super(message, cause);
    }

    public MyException(String message) {
    }
}
