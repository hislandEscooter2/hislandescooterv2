package com.hisland.backend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.messaging.handler.annotation.MessageMapping;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class LocationController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocationController.class);

    private final SimpMessagingTemplate template;

    @Autowired
    LocationController(SimpMessagingTemplate template){
        this.template = template;
    }

    @MessageMapping("/send/message")
    public void onReceivedMesage(String message){
        LOGGER.info("gps call" + message);
        this.template.convertAndSend("/Gps",  new SimpleDateFormat("yyyy:HH:mm:ss").format(new Date())+" - "+ message);
    }
}
