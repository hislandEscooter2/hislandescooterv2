package com.hisland.backend.repository;

import com.hisland.backend.model.GPSCoordinate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CoordinatesRepo extends JpaRepository<GPSCoordinate, Integer> {
}
