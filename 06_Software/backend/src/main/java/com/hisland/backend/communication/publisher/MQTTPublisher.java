package com.hisland.backend.communication.publisher;

import com.hisland.backend.communication.receiver.MQTTReceiver;
import com.hisland.backend.exception.CommunicationException;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

/**
 * class that publishes to the mqtt publisher and handles errors
 */
@Component
public class MQTTPublisher implements Publisher {
    private static final Logger LOGGER = LoggerFactory.getLogger(MQTTPublisher.class);

    private final
    SimpMessagingTemplate template;

    private final
    MQTTReceiver mqttReceiver;

    MqttMessage msg = new MqttMessage();

    @Autowired
    public MQTTPublisher(SimpMessagingTemplate template, MQTTReceiver mqttReceiver) {
        this.template = template;
        this.mqttReceiver = mqttReceiver;
    }


    /**
     * publishes data to the mqtt broker
     * @param MQTTMessageData
     * @param topic
     * @throws CommunicationException
     */
    public void publish(String MQTTMessageData,String topic) throws CommunicationException{
        LOGGER.info("MQTTPublisher IS ACTIVATED");

        msg.clearPayload();
        msg.setPayload(MQTTMessageData.getBytes());
        try {
            if(mqttReceiver.getMqttClient().isConnected()){
                mqttReceiver.getMqttClient().publish(topic, msg);
            }else {
                mqttReceiver.reconnectClient();
                LOGGER.info("Error handled");
                this.publish(MQTTMessageData, topic);
            }
        } catch (MqttException e) {
            throw new CommunicationException(e.getMessage(), e);
        }
    }
}
