package com.hisland.backend.model;

public class Distance {
    private String value;
    private String text;


    // Getter Methods

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    // Setter Methods

    public void setValue(String value) {
        this.value = value;
    }

    public void setText(String text) {
        this.text = text;
    }
}
