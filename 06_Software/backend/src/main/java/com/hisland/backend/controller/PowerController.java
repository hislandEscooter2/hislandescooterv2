package com.hisland.backend.controller;

import com.hisland.backend.communication.processing.PowerProcessor;
import com.hisland.backend.exception.CommunicationException;
import com.hisland.backend.service.ProcessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Controller
public class PowerController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PowerController.class);

    private final
    ProcessService processService;
    private final
    PowerProcessor powerProcessor;

    @Autowired
    public PowerController(ProcessService processService, PowerProcessor powerProcessor) {
        this.processService = processService;
        this.powerProcessor = powerProcessor;
    }

    @PostMapping("/power")
    public ResponseEntity powerCall(@RequestBody String bool) throws CommunicationException {  // TODO: 16/05/2019  error handling

        LOGGER.info("power call" + bool);
        if (bool.toLowerCase().equals("0")||bool.toLowerCase().equals("1")){
            processService.setPowerProcessor(bool);
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/power")
    private ResponseEntity<String> getPower() {
        LOGGER.info("power call");
        return new ResponseEntity(String.valueOf(powerProcessor.getPower()), HttpStatus.OK);
    }


}
